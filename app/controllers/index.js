/*
 * Tutorials: https://www.authlete.com/documents/apigateway_lambda_oauth
 * DOCS - http://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_Operations.html
 * TUT - https://snowulf.com/2015/08/05/tutorial-aws-api-gateway-to-lambda-to-dynamodb
 */

/*
 * Include the aws calls in the lib folder
 */
var api = require('awsGatewayApi');

/*
 * Read data from the amazon api.
 */
function awsRead() {

	api.getItems({
		"device" : "test",
		"datetime" : "1446757444524"
	}, function(_response) {
		console.log("Running a api.getItems");
		console.log(_response);
		$.data.text = JSON.stringify(_response);
	});

}

/*
 * Read a single item from amazon api
 */
function awsReadSingle() {

	api.getItem({
		"device" : "test",
		"datetime" : "1446757444524"
	}, function(_response) {
		console.log("Running a api.getItem");
		console.log(_response);
		$.data.text = JSON.stringify(_response);
	});

}

/*
 * Updates a item using the amazon api
 */
function awsUpdateItem() {

	api.updateItem({
		"device" : "test",
		"datetime" : "1446757444524",
		"field" : "latitude",
		"value" : "092837489"
	}, function(_response) {
		console.log("Running a api.updateItem");
		console.log(_response);
		$.data.text = JSON.stringify(_response);
	});

}

$.index.open();
