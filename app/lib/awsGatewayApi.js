
/*
 * Get item calls
 * $params url, callback
 */
exports.getItem = function(data, _callback) {

	var xhr = Ti.Network.createHTTPClient({
		onload : function(e) {

			console.log(this);

			_callback({
				success : true,
				data : JSON.parse(this.responseText)
			});
		},
		onerror : function(e) {
			// this function is called when an error occurs, including a timeout
			Ti.API.error(e.error);
			_callback({
				success : false,
				error : e
			});
		},
		timeout : 5000 /* in milliseconds */
	});
	xhr.open("POST", "https://tibqwxuqoh.execute-api.us-east-1.amazonaws.com/dev/getitem");
	xhr.send(JSON.stringify(data));

};

/**
 * THIS FUNCTION IS NOT FOR APPCELERATOR AND SHOUILD BE ADDED TO YOU LAMBDA SETUP
 * Dynamo DB crud operations for Lambda
 * @type {[type]}
 * DOCS - http://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_Operations.html
 * TUT - https://snowulf.com/2015/08/05/tutorial-aws-api-gateway-to-lambda-to-dynamodb
 */
var AWS = require('aws-sdk');
var dynamoDB = new AWS.DynamoDB();

exports.handler = function(event, context) {
    
    var tableName = "bill";
    var datetime = new Date().getTime().toString();
    
    var getData = {
        "TableName": tableName,
        "Key": {
            "device": {
                "S": (event.device === undefined ? "test" : event.device) 
            },
            "datetime": {
                "N": (event.datetime === undefined ? "1446757400919" : event.datetime) 
            }
        }
    };
    dynamoDB.getItem(getData, function(err, data) {
        if (err) {
            context.fail('ERROR: Dynamo failed: ' + err);
        }else{
            console.log('Dynamo Success: ' + JSON.stringify(data, null, '  '));
            context.done(null, data.Item);
        }
    });

};

/*
 * Get item calls
 * $params url, callback
 */
exports.getItems = function(data, _callback) {

	var xhr = Ti.Network.createHTTPClient({
		onload : function(e) {

			console.log(this);

			_callback({
				success : true,
				data : JSON.parse(this.responseText)
			});
		},
		onerror : function(e) {
			// this function is called when an error occurs, including a timeout
			Ti.API.error(e.error);
			_callback({
				success : false,
				error : e
			});
		},
		timeout : 5000 /* in milliseconds */
	});
	xhr.open("POST", "https://tibqwxuqoh.execute-api.us-east-1.amazonaws.com/dev/getitems");
	xhr.send(JSON.stringify(data));

};

/**
 * THIS FUNCTION IS NOT FOR APPCELERATOR AND SHOUILD BE ADDED TO YOU LAMBDA SETUP
 * Dynamo DB crud operations for Lambda
 * @type {[type]}
 * DOCS - http://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_Operations.html
 * TUT - https://snowulf.com/2015/08/05/tutorial-aws-api-gateway-to-lambda-to-dynamodb
 */
var AWS = require('aws-sdk');
var dynamoDB = new AWS.DynamoDB();

exports.handler = function(event, context) {
 
    /**
     * Debugging events
     * @type {[type]}
     */
    console.log("Request received:\n", JSON.stringify(event));
    console.log("Context received:\n", JSON.stringify(context));
    
    /**
     * Important this needs to be your Dynamo DB table name
     * @type {String}
     */
    var tableName = "bill";
    var datetime = new Date().getTime().toString();
    
    /**
     * Read results
     * DOCS - http://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_Scan.html
     */
    var scanData = {
        "TableName" : tableName,
        "Limit" : 10
    };
    dynamoDB.scan(scanData, function(err, data) {
        if (err) {
            context.done('error','reading dynamodb failed: '+err);
        }
        context.done(null, data.Items);
    });

};

/*
 * update Item
 * $params url, callback
 */
exports.updateItem = function(data, _callback) {

	var xhr = Ti.Network.createHTTPClient({
		onload : function(e) {

			console.log(this);

			_callback({
				success : true,
				data : JSON.parse(this.responseText)
			});
		},
		onerror : function(e) {
			// this function is called when an error occurs, including a timeout
			Ti.API.error(e.error);
			_callback({
				success : false,
				error : e
			});
		},
		timeout : 5000 /* in milliseconds */
	});
	xhr.open("POST", "https://tibqwxuqoh.execute-api.us-east-1.amazonaws.com/dev/updateitem");
	xhr.send(JSON.stringify(data));

}; 

/**
 * THIS FUNCTION IS NOT FOR APPCELERATOR AND SHOUILD BE ADDED TO YOU LAMBDA SETUP
 * Dynamo DB crud operations for Lambda
 * @type {[type]}
 * DOCS - http://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_Operations.html
 * TUT - https://snowulf.com/2015/08/05/tutorial-aws-api-gateway-to-lambda-to-dynamodb
 */
var AWS = require('aws-sdk');
var dynamoDB = new AWS.DynamoDB();

exports.handler = function(event, context) {
 
    /**
     * Debugging events
     * @type {[type]}
     */
    console.log("Request received:\n", JSON.stringify(event));
    console.log("Context received:\n", JSON.stringify(context));
    
    /**
     * Important this needs to be your Dynamo DB table name
     * @type {String}
     */
    var tableName = "bill";
    var datetime = new Date().getTime().toString();
    
    /**
     * Update a item
     * DOCS - http://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_UpdateItem.html
     */
    var updateData = {
        "TableName": tableName,
        "Key": {
            "device": {
                "S": event.device
            },
            "datetime": {
                "N": event.datetime
            }
        },
        "UpdateExpression": "set " + event.field + " = :val1",
       
        "ExpressionAttributeValues": {
            ":val1": {"N": event.value},

        },
        "ReturnValues": "ALL_NEW"
    };
    dynamoDB.updateItem(updateData, function(err, data) {
        if (err) {
            context.fail('ERROR: Dynamo failed: ' + err);
        } else {
            console.log('Dynamo Success: ' + JSON.stringify(data, null, '  '));
            context.succeed(data);
        }
    });

};